function setcellbasepref(pref_name,pref_value)
%SETCELLBASEPREF   Change CellBase preference.
%   SETCELLBASEPREF(NAME,VALUE) sets CellBase preference NAME to VALUE.
%
%   See also RELOCATECB, INITCB, CHOOSECB, RENAMECB and DELETECB.

%   Edit log: BH 7/7/20

% Check input arguments
narginchk(2,2)

% Set preference
setpref('cellbase',pref_name,pref_value)

% Set preference in 'cellbases'
cellbases = getpref('cellbase','cellbases');
crcb = findcb(getpref('cellbase','name'));
cellbases{crcb}.(pref_name) = pref_value;
setpref('cellbase','cellbases',cellbases)