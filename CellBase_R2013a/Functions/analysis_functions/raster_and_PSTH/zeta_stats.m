function [dblZetaP vecLatencies sZETA sRate] = zeta_stats(cellid, event, varargin)
%ZETA_STATS Calculates neuronal responsiveness index zeta
%   [DBLZETAP VECLATENCIES SZETA SRATE] = ZETA_STATS(CELLID, EVENT, VARARGIN)
%	
%    Mandatory input arguments:
%       CELLID: Defines the cell
%       EVENT: the event that marks the beginning of the window analysed
%
%   Default behavior of ZETA_STATS can be modified by using a set of
%   paramter-value pairs as optional input parameters. The following
%   parameters are implemented (with default values):	
%       'dblUseMaxDur': float (s), window length for calculating ZETA: ignore all spikes beyond this duration after event onset
%								[default: minimum of all event onsets to next event onset]
%       'intResampNum': integer, number of resamplings (default: 100)
%       'intPlot': integer, plotting switch (0=none, 1=inst. rate only, 2=traces only, 3=raster plot as well, 4=adds latencies in raster plot) (default: 0)
%       'intLatencyPeaks': integer, maximum number of latency peaks to return (1-4) (default: 2)
%       'vecRestrictRange': temporal range within which to restrict onset/peak latencies (default: [-inf inf])
%       'boolDirectQuantile': boolean, switch to use the empirical null-distribution rather than the
%								Gumbel approximation (default: false) [Note: requires many resamplings!]
%       'dblJitterSize': scalar, sets the temporal jitter window relative to dblUseMaxDur (default: 2)
%
%	output:
%       'dblZetaP'; p-value based on Zenith of Event-based Time-locked Anomalies
%       'vecLatencies'; different latency estimates, number determined by intLatencyPeaks. If no peaks are detected, it returns NaNs:
%           1) Latency of ZETA
%           2) Latency of largest z-score with inverse sign to ZETA
%           3) Peak time of instantaneous firing rate
%           4) Onset time of above peak, defined as the first crossing of peak half-height
%       'sZETA'; structure with fields:
%           - dblZETA; responsiveness z-score (i.e., >2 is significant)
%           - dblD; temporal deviation value underlying ZETA
%           - dblP; p-value corresponding to ZETA
%           - dblPeakT; time corresponding to ZETA
%           - intPeakIdx; entry corresponding to ZETA
%           - dblMeanD; Cohen's D based on mean-rate stim/base difference
%           - dblMeanP; p-value based on mean-rate stim/base difference
%           - vecSpikeT: timestamps of spike times (corresponding to vecD)
%           - vecD; temporal deviation vector of data
%           - matRandD; baseline temporal deviation matrix of jittered data
%           - dblD_InvSign; largest peak of inverse sign to ZETA (i.e., -ZETA)
%           - dblPeakT_InvSign; time corresponding to -ZETA
%           - intPeakIdx_InvSign; entry corresponding to -ZETA
%           - dblUseMaxDur; window length used to calculate ZETA
%       'sRate'; structure with fields: (only if intLatencyPeaks > 0)
%           - vecRate; instantaneous spiking rates (like a PSTH)
%           - vecT; time-points corresponding to vecRate (same as sZETA.vecSpikeT)
%           - vecM; Mean of multi-scale derivatives
%           - vecScale; timescales used to calculate derivatives
%           - matMSD; multi-scale derivatives matrix
%           - vecV; values on which vecRate is calculated (same as sZETA.vecZ)
%       Data on the peak:
%           - dblPeakTime; time of peak (in seconds)
%           - dblPeakWidth; duration of peak (in seconds)
%           - vecPeakStartStop; start and stop time of peak (in seconds)
%           - intPeakLoc; spike index of peak (corresponding to sZETA.vecSpikeT)
%           - vecPeakStartStopIdx; spike indices of peak start/stop (corresponding to sZETA.vecSpikeT)
%		Additionally, it will return peak onset latency (first crossing of peak half-height) using getOnset.m:
%           - dblOnset: latency for peak onset
%
prs = inputParser;
addRequired(prs,'cellid',@(s)iscellid(s)|issessionid(s))
addRequired(prs,'event',@(s)ischar(s))   % reference event
%optional arguments
addParamValue(prs,'dblUseMaxDur','none',@(s)ischar(s)|iscellstr(s))
addParamValue(prs,'intResampNum',100)
addParamValue(prs,'intPlot',0,@(s)ismember(s, [0 1 2 3 4]))
addParamValue(prs,'intLatencyPeaks',2,@(s)ismember(s, [1 2 3 4]))
addParamValue(prs,'vecRestrictRange',[-inf, inf],@(s)isa(s, 'double')&&size(s)==[1 2]|size(s)==[2 1])
addParamValue(prs,'boolDirectQuantile',false,@islogical)
addParamValue(prs,'dblJitterSize',2,@isscalar)
parse(prs,cellid,event,varargin{:})
g = prs.Results;

stimes = loadcb(cellid,'Spikes');
stimes = stimes(:); %spike times in a collumn vector

TE = loadcb(cellid, 'TrialEvents');
etimes = TE.(event) + TE.TrialStart; %event start times
etimes = etimes(~isnan(etimes)); %filter out valid trials
etimes = etimes(:); %must be a collumn vector

%set default window length
if strcmp(g.dblUseMaxDur, 'none')
   g.dblUseMaxDur = min(diff(etimes(:))); %shortest time between events
end

[dblZetaP vecLatencies sZETA sRate] = getZeta(stimes,etimes, g.dblUseMaxDur,g.intResampNum,g.intPlot,g.intLatencyPeaks,g.vecRestrictRange,g.boolDirectQuantile);

end